<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Dish
 *
 * @ORM\Table(name="dish")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DishRepository")
 * @Vich\Uploadable
 */
class Dish
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank(message="Название не должно быть пустым")
     * @Assert\Length(max="60", maxMessage="Длина названия не должна превышать {{ limit }} символов.")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=4)
     * @Assert\NotBlank(message="Поле цена не должно быть пустым")
     * @Assert\Type(
     *     type="numeric",
     *     message="Значение {{ value }} не является валидным форматом."
     * )
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="imageName", type="string", length=255)
     */
    private $imageName;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="dish_img", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @var Place
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Place", inversedBy="dishes")
     */
    private $place;

    /**
     * @var Purchase[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Purchase", mappedBy="dish")
     */
    private $purchases;

    public function __construct()
    {
        $this->purchases = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Dish
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Dish
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set imageName
     *
     * @param string $imageName
     *
     * @return Dish
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * Get imageName
     *
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param File $imageFile
     * @return Dish
     */
    public function setImageFile(File $imageFile): Dish
    {
        $this->imageFile = $imageFile;
        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile(): File
    {
        return $this->imageFile;
    }

    /**
     * @param Place $place
     * @return Dish
     */
    public function setPlace(Place $place): Dish
    {
        $this->place = $place;
        return $this;
    }

    /**
     * @return Place
     */
    public function getPlace(): Place
    {
        return $this->place;
    }

    /**
     * @return Purchase[]|ArrayCollection
     */
    public function getPurchases()
    {
        return $this->purchases;
    }

    /**
     * @param Purchase $purchase
     */
    public function addPurchase(Purchase $purchase)
    {
        $this->purchases->add($purchase);
    }

    /**
     * @param Purchase $purchase
     */
    public function removePurchase(Purchase $purchase)
    {
        $this->purchases->removeElement($purchase);
    }
}

