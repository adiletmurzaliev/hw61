<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Dish;
use AppBundle\Entity\Place;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class BasicController extends Controller
{
    /**
     * @Route("/", name="basic_index")
     * @Method("GET")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        $data = $this
            ->getDoctrine()
            ->getRepository(Dish::class)
            ->getPopularDishesByPlaces(2);

        return $this->render('@App/Basic/index.html.twig', [
            'data' => $data
        ]);
    }

    /**
     * @Route("/{id}/menu", requirements={"id" : "\d+"}, name="basic_menu")
     * @Method("GET")
     *
     * @param Place $place
     * @return Response
     */
    public function menuAction(Place $place): Response
    {
        $dishes = $this->getDoctrine()->getRepository(Dish::class)->findBy(['place' => $place]);

        return $this->render('@App/Basic/menu.html.twig', [
            'place' => $place,
            'dishes' => $dishes
        ]);
    }

}
