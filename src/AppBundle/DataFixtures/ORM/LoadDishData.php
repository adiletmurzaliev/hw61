<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Dish;
use AppBundle\Entity\Place;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class LoadDishData extends Fixture implements DependentFixtureInterface
{
    private static $dishes = [];
    private $menu = [];

    public function load(ObjectManager $manager)
    {
        $this->clearImages();
        $this->generateMenu();

        foreach ($this->menu as $placeId => $placeMenu) {
            foreach ($placeMenu as $id => $item) {
                $dish = new Dish();
                /** @var Place $place */
                $place = $this->getReference($placeId);

                $dish
                    ->setTitle($item['title'])
                    ->setPrice($item['price'])
                    ->setImageFile(new UploadedFile($item['image'], $item['title'], null, null, null, true))
                    ->setPlace($place);

                $manager->persist($dish);

                $dishIdent = "dish{$id}{$placeId}";
                $this->addReference($dishIdent, $dish);
                self::$dishes[] = $dishIdent;
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            LoadPlaceData::class
        );
    }

    public static function getDishes()
    {
        return self::$dishes;
    }

    private function generateMenu()
    {
        $fixturesPath = __DIR__ . '/../../../../web/fixtures/';

        $this->menu = [
            LoadPlaceData::PLACE_ONE => [
                [
                    'title' => 'Ассорти шашлык',
                    'price' => 300,
                    'image' => $fixturesPath . 'sarya_assorti.jpg'
                ],
                [
                    'title' => 'Сария шашлык',
                    'price' => 230,
                    'image' => $fixturesPath . 'sarya_shashlik.jpg'
                ],
                [
                    'title' => 'Оливье',
                    'price' => 180,
                    'image' => $fixturesPath . 'sarya_olive.jpg'
                ],
                [
                    'title' => 'Ган-фан',
                    'price' => 200,
                    'image' => $fixturesPath . 'sarya_ganfan.jpg'
                ],
                [
                    'title' => 'Карточель Фри',
                    'price' => 100,
                    'image' => $fixturesPath . 'sarya_fri.jpg'
                ]
            ],
            LoadPlaceData::PLACE_TWO => [
                [
                    'title' => 'Шашлык',
                    'price' => 250,
                    'image' => $fixturesPath . 'faiza_shashlik.jpg'
                ],
                [
                    'title' => 'Бризоль',
                    'price' => 230,
                    'image' => $fixturesPath . 'faiza_brizol.jpg'
                ],
                [
                    'title' => 'Пельмени',
                    'price' => 160,
                    'image' => $fixturesPath . 'faiza_pelmeni.jpg'
                ],
                [
                    'title' => 'Лагман',
                    'price' => 180,
                    'image' => $fixturesPath . 'faiza_lagman.jpg'
                ],
                [
                    'title' => 'Манты',
                    'price' => 180,
                    'image' => $fixturesPath . 'faiza_manti.jpg'
                ]
            ]
        ];

        // Copying fixtures images
        foreach ($this->menu as $placeId => $placeMenu) {
            foreach ($placeMenu as $dishId => $dish) {
                $newImageName = $fixturesPath . $dish['title'] . '.jpg';
                copy($dish['image'], $newImageName);
                $this->menu[$placeId][$dishId]['image'] = $newImageName;
            }
        }
    }

    private function clearImages()
    {
        $files = glob(__DIR__ . '/../../../../web/images/dishes/*');
        foreach ($files as $file) {
            if (is_file($file))
                unlink($file);
        }
    }
}