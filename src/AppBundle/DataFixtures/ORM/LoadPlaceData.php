<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Place;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class LoadPlaceData extends Fixture
{
    const PLACE_ONE = 'Sarya';
    const PLACE_TWO = 'Faiza';

    public function load(ObjectManager $manager)
    {
        $this->clearImages();
        $images = $this->generateImages();

        $place1 = new Place();
        $place1
            ->setTitle('Кафе «Сария»')
            ->setDescription('Кафе «Сария» находится в южной части Бишкека. Мы открыты для вас 24 часа в сутки. Приятный интерьер, великолепные напитки и кухня, помогут Вам расслабится и отдохнуть.')
            ->setImageFile(new UploadedFile($images['place1'], 'place1.jpg', null, null, null, true));

        $manager->persist($place1);

        $place2 = new Place();
        $place2
            ->setTitle('Кафе «Фаиза»')
            ->setDescription('«Фаиза» — кафе с великолепными блюдами восточной кухни по очень хорошим ценам. Здесь всегда много людей. Отличное место, чтобы насладиться среднеазиатской кухней. Стоит посетить обязательно!')
            ->setImageFile(new UploadedFile($images['place2'], 'place2.jpg', null, null, null, true));

        $manager->persist($place2);
        $manager->flush();

        $this->addReference(self::PLACE_ONE, $place1);
        $this->addReference(self::PLACE_TWO, $place2);
    }

    private function generateImages(): array
    {
        $fixturesPath = __DIR__ . '/../../../../web/fixtures/';

        // Copying fixtures images
        copy($fixturesPath . 'sarya.jpg', $fixturesPath . 'place1.jpg');
        copy($fixturesPath . 'faiza.jpg', $fixturesPath . 'place2.jpg');

        return [
            'place1' => $fixturesPath . 'place1.jpg',
            'place2' => $fixturesPath . 'place2.jpg'
        ];
    }

    private function clearImages()
    {
        $files = glob(__DIR__ . '/../../../../web/images/places/*');
        foreach ($files as $file) {
            if (is_file($file))
                unlink($file);
        }
    }
}
