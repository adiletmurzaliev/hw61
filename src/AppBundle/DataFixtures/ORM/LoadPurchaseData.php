<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Dish;
use AppBundle\Entity\Purchase;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;


class LoadPurchaseData extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $arrayDishes = LoadDishData::getDishes();

        for ($i = 0; $i < 1000; $i++) {
            $purchase = new Purchase();

            $date = new \DateTime(date("Y-m-d H:i:s", time() - rand(0, 31536000 + 400000)));
            $j = (int)floor($i / 100);

            /** @var Dish $dish */
            $dish = $this->getReference($arrayDishes[$j]);

            $purchase
                ->setDate($date)
                ->setDish($dish);

            $manager->persist($purchase);
        }

        $manager->flush();
    }

    function getDependencies()
    {
        return array(
            LoadDishData::class
        );
    }

}